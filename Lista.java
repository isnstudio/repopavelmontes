package Actividades;

import java.util.ArrayList;
import java.util.List;

public class Lista {
    public static void main(String[] args) {
        List<Integer> iList = new ArrayList<Integer>(5);

        imputList(iList);
        System.out.println("Lista sin ordenar");
        impList(iList);
        outList(iList);
        System.out.println("Lista Ordenada");
        impList(iList);
    }

    private static void imputList( List<Integer> list){ //Crea lista al azar.
        for (int i = 0; i < 5; i++) {
            list.add( (int) (Math.random()*10));
        }
    }

    private static void outList( List<Integer> list){ //Mueve de posicion.
        int val;
        for (int i = 0; i<list.size(); i++){
            for (int j = i + 1; j <list.size(); j++){

                if ( list.get(i) > list.get(j)){

                    val = list.get(i);
                    list.set(i ,list.get(j));
                    list.set (j , val);
                }


            }
        }
    }
    private static void impList ( List<Integer> list ){ //Imprime lista.
        for (int i = 0; i < list.size(); i++) {
                System.out.println("| " + list.get(i) + " |");
        }
    }

}

