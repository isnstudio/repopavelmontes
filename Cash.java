package Actividades;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Cash {
    public static void main(String[] args) {
        Scanner imput = new Scanner(System.in).useLocale(Locale.US);

        System.out.print("Ingresa una cantidad de dinero = ");
        double dMoney = imput.nextDouble();


        System.out.println(checkMoney(dMoney));
    }

    private static String checkMoney (double dMoney){ // Metodo
        DecimalFormat Format = new DecimalFormat("#.00");
        int comparacion [] = {500 , 200 , 100 , 50 , 20 , 10 , 5 ,2 , 1}; // Arreglo con la cantidad de dinero que se va a comparar

        //---------------
        String sValue = "";
        double dRestos = dMoney;
        //----------------

        for (int i = 0; i < comparacion.length ; i++) {

            if (dRestos >= comparacion[i]){
                //System.out.println(i  +  "," + comparacion[i]);
                sValue += " $" + comparacion[i] + " = " + ((int) dRestos / comparacion[i]);
                dRestos = dRestos % comparacion [i];
            }
        }

        if (dRestos != 0)
            sValue += " $" + Format.format(dRestos);

        return sValue;
    }
}
